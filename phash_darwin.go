package phash

// #cgo CXXFLAGS: -I/usr/local/opt/libjpeg/include -I/usr/local/opt/libpng/include -I/usr/local/opt/libtiff/include -I/opt/homebrew/include
// #cgo LDFLAGS: -L/usr/local/opt/libjpeg/lib -L/usr/local/opt/libpng/lib -L/usr/local/opt/libtiff/lib -L/opt/homebrew/lib
import "C"
