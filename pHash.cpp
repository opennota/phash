#define cimg_use_jpeg
#define cimg_use_png
#define cimg_use_tiff
#define cimg_use_heif
#include <stdio.h>
#include "c++/pHash/pHash.cpp"

ulong64
ph_dct_imagehash_wrapper_cxx(const char *file) {
	ulong64 hash;
	try {
		if (ph_dct_imagehash(file, hash) == 0) {
			errno = 0;
		}
	} catch(...) {
		fprintf(stderr, "failed to decode %s\n", file);
		return 0;
	}
	return hash;
}

#ifdef __cplusplus
extern "C" {
#endif

void
cimg_exception_mode_quiet() {
	cimg::exception_mode(0);
}

ulong64
ph_dct_imagehash_wrapper(const char *file) {
	return ph_dct_imagehash_wrapper_cxx(file);
}

#ifdef __cplusplus
}
#endif
